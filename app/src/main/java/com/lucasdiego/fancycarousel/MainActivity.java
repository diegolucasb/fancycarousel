package com.lucasdiego.fancycarousel;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.GrayscaleTransformation;

public class MainActivity extends AppCompatActivity {

    private static final int VISIBLE_STACK_ITENS = 4;
    private static final int MARGIN_BETWEEN_OBJECTS = 40;

    @BindView(R.id.content)
    RelativeLayout content;

    List<Device> deviceList = new ArrayList<>();
//    Stack<Device> deviceList = new Stack<>();

    List<StackViewHolder> listStackViewHolder = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        deviceList.add(new Device(1, R.drawable.carro1, 80, "Delorean VT200", "Em casa"));
        deviceList.add(new Device(2, R.drawable.carro2, 80, "Relógio Azul", "No Trabalho"));

        deviceList.add(new Device(3, R.drawable.carro3, 80, "HB-20 Preto", "Em casa"));
        deviceList.add(new Device(4, R.drawable.carro4, 80, "Clio Branco", "Em casa"));
        deviceList.add(new Device(5, R.drawable.carro5, 80, "Carro de Fulano", "Em casa"));
        deviceList.add(new Device(6, R.drawable.car_default_image_list, 80, "Moto de Cicrano", "Em casa"));

        drawStack();
    }

    private void drawDeviceOnStack(Device device){


    }

    private void drawStack(){
        int lastViewId = 0;
        for (int i = 0; i < deviceList.size(); i++){

            View deviceStackView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.device_image_stack_item, null);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            if(i > (deviceList.size() - VISIBLE_STACK_ITENS)){
                int marginLeft = MARGIN_BETWEEN_OBJECTS;
                params.setMargins(marginLeft, 0, 0, 0);
            }

            if(i > 0){
                params.addRule(RelativeLayout.ALIGN_LEFT, lastViewId);
            }

            lastViewId = View.generateViewId();
            deviceStackView.setId(lastViewId);

            deviceStackView.setLayoutParams(params);
            final StackViewHolder viewHolder = new StackViewHolder(deviceStackView, deviceList.get(i));
            viewHolder.device = deviceList.get(i);
            viewHolder.loadImages(i == deviceList.size() - 1, i==0);//se é o ultimo device da stack
            listStackViewHolder.add(viewHolder);

            content.addView(viewHolder.view);

            //last view adds, name and location view
            if(i == deviceList.size() - 1){

                View deviceStackText = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.device_text_stack_item, null);

                RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                params1.addRule(RelativeLayout.RIGHT_OF, lastViewId);
                params1.setMargins(10, 10, 0, 0);

                deviceStackText.setLayoutParams(params1);

                StackTitleViewHolder textViewHolder = new StackTitleViewHolder(deviceStackText);
                textViewHolder.title.setText(deviceList.get(i).getName());
                textViewHolder.location.setText(deviceList.get(i).getLocation());

                content.addView(textViewHolder.view);
            }

        }
    }

    private void reorderStack() {
//        Collections.rotate(deviceList, -1);
//        drawStack();

        moveStackListAnimation();
    }

    private void putItOnFirst(Device device){
        int index = deviceList.indexOf(device);
        deviceList.remove(index);
        deviceList.add(device);
        drawStack();
    }

    private Rect getRect(View view){
        int[] location = new int[2];
        view.getLocationInWindow(location);
        return new Rect(
                location[0],
                location[1],
                location[0] + view.getWidth(),
                location[1] + view.getHeight()
        );


    }

    private void moveStackListAnimation(){
//        Collections.rotate(deviceList, -1);

        List<StackViewHolder> auxList = new ArrayList<>();
        auxList.addAll(listStackViewHolder);

        StackViewHolder first = auxList.get(0);
        StackViewHolder second = auxList.get(1);
        StackViewHolder last = auxList.get(auxList.size()-1);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 0);

        ((RelativeLayout.LayoutParams)second.view.getLayoutParams()).removeRule(RelativeLayout.ALIGN_LEFT);
        second.view.setLayoutParams(params);

        for(int i = auxList.size()-1; i >= deviceList.size() - (VISIBLE_STACK_ITENS - 1); i--){

            StackViewHolder viewHolder = auxList.get(i);
            viewHolder.setImageGrayScale();

            int newWidth = getRect(viewHolder.view).left;
            ObjectAnimator animator = ObjectAnimator.ofFloat(viewHolder.view, "x", newWidth - MARGIN_BETWEEN_OBJECTS);
            animator.setDuration(1000);
            animator.start();
        }

//        first.view.setVisibility(View.INVISIBLE);
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params1.setMargins(0, 0, 0, 0);
        params1.addRule(RelativeLayout.ALIGN_LEFT, last.view.getId());

        ((RelativeLayout.LayoutParams)first.view.getLayoutParams()).removeRule(RelativeLayout.ALIGN_LEFT);

        first.view.setLayoutParams(params1);
        first.loadImages(true, false);
        first.view.setVisibility(View.VISIBLE);
        first.view.bringToFront();
//        first.fadeIn();

        //rotaciona alista de stackViewHOlder
        Collections.rotate(listStackViewHolder, -1);
    }

    private void rotate(List listStackViewHolder) {
        listStackViewHolder.add(0, listStackViewHolder.get(listStackViewHolder.size()-1));
        listStackViewHolder.remove(listStackViewHolder.size()-1);
    }


    public class Device{

        private int id;
        private int drawableImage;
        private int batteryLevel;
        private String name;
        private String location;

        public Device(int id, int drawableImage, int batteryLevel, String name, String location) {
            this.id = id;
            this.drawableImage = drawableImage;
            this.batteryLevel = batteryLevel;
            this.name = name;
            this.location = location;
        }

        public int getDrawableImage() {
            return drawableImage;
        }

        public void setDrawableImage(int drawableImage) {
            this.drawableImage = drawableImage;
        }

        public int getBatteryLevel() {
            return batteryLevel;
        }

        public void setBatteryLevel(int batteryLevel) {
            this.batteryLevel = batteryLevel;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Device device = (Device) o;

            return id == device.id;

        }

        @Override
        public int hashCode() {
            return id;
        }
    }

    public class StackViewHolder{

        private View view;
        private Device device;

        @BindView(R.id.imageCircle)
        ImageView imageCircle;

        @BindView(R.id.imageBattery)
        ImageView imageBattery;

        private Animation fadeIn;
        private Animation fadeOut;

        public StackViewHolder(View view, Device device){
            this.view = view;
            this.device = device;
            ButterKnife.bind(this, view);
        }

        private void fadeIn(){
            fadeIn = AnimationUtils.loadAnimation(view.getContext(), R.anim.fade_in);
            view.startAnimation(fadeIn);
        }

        private void fadeOut(){
            fadeOut = AnimationUtils.loadAnimation(view.getContext(), R.anim.fade_out);
            view.setAnimation(fadeOut);
            view.animate().setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    view.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            }).start();
        }

        private void setImageGrayScale(){
            Glide.with(view.getContext())
                    .load(device.getDrawableImage())
                    .apply(RequestOptions.bitmapTransform(new GrayscaleTransformation()))
                    .into(imageCircle);

            VectorDrawableCompat bateryVector = VectorDrawableCompat.create(
                    view.getContext().getResources(),
                    R.drawable.ic_battery_inactive,
                    view.getContext().getTheme());

            imageBattery.setImageDrawable(bateryVector);

        }

        private void loadImages(final boolean firstOfStack, boolean lastOfStack){

//            view.setAnimation(fadeIn);
            if(firstOfStack){
                Glide.with(view.getContext())
                        .load(device.getDrawableImage())
                        .into(imageCircle);


            }else{
                setImageGrayScale();
            }

            VectorDrawableCompat bateryVector = VectorDrawableCompat.create(
                    view.getContext().getResources(),
                    (firstOfStack?R.drawable.ic_battery_full:R.drawable.ic_battery_inactive),
                    view.getContext().getTheme());

            imageBattery.setImageDrawable(bateryVector);

//            view.animate().alpha(1).setDuration(1000).setInterpolator(new DecelerateInterpolator()).withEndAction(new Runnable() {
//                @Override
//                public void run() {
//                    view.animate().alpha(0).setDuration(1000).setInterpolator(new AccelerateInterpolator()).start();
//                }
//            }).start();

        }

        public void animateLastView() {
            view.animate();
        }
    }

    public class StackTitleViewHolder implements View.OnClickListener{

        private View view;

        @BindView(R.id.deviceName)
        TextView title;

        @BindView(R.id.location)
        TextView location;

        public StackTitleViewHolder(View view){
            this.view = view;
            ButterKnife.bind(this, view);

            this.view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //change stack position
            reorderStack();
        }
    }


}
